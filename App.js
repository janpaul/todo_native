/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Button,
  Text,
  StatusBar,
} from 'react-native';
import 'react-native-gesture-handler';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import HeaderLayout from './components/Header';
import {Switch, Route, Router, BrowserRouter} from 'react-router-dom';

import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';
import Dashboard from './components/Dashboard';
import AddTodo from './components/AddTodo';
import Login from './components/Login';
import Register from './components/Register';

const Stack = createStackNavigator();
const App: () => React$Node = () => {
  return (
    <>
      <NavigationContainer>
        <Stack.Navigator>
          <Stack.Screen
            name="Login"
            component={Login}
            options={{title: 'Login'}}
          />
          <Stack.Screen
            name="Register"
            component={Register}
            options={{title: 'Create Account'}}
          />
          <Stack.Screen
            name="Dashboard"
            component={Dashboard}
            options={{title: 'My Todo List'}}
          />
          <Stack.Screen
            name="Add"
            component={AddTodo}
            options={{title: 'Add Todo'}}
          />
        </Stack.Navigator>
        {/* <HeaderLayout /> */}
      </NavigationContainer>
    </>
  );
};

export default App;
