import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  Image,
  Button,
  TextInput,
  StatusBar,
  CheckBox,
} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {todo, create, update} from './Url';
const Dashboard = ({navigation, route}) => {
  const {token} = route.params;

  const [todo_, setTodo] = useState('');

  const [todo_list, setList] = useState([]);

  const submit = () => {
    create(token, todo_).then((r) => {
      // todo(token);
      setTodo('');
      fetch();
    });
  };

  const mark_done = (id) => {
    update(token, id).then((r) => {
      fetch();
    });
  };

  const render_list = () => {
    return todo_list.map((list, idx) => (
      <TouchableOpacity onPress={() => mark_done(list.id)} key={idx}>
        <View style={styles.todo}>
          {list.is_done === "true" ? (
            <CheckBox value={true} />
          ) : (
            <CheckBox value={false} />
          )}
          <Text style={styles.todo_text} idx={idx}>
            {list.todo}
          </Text>
        </View>
      </TouchableOpacity>
    ));
  };
  fetch = () => {
    todo(token)
      .then((r) => {
        setList(r.data.todos);
        console.log(r.data);
      })
      .catch((e) => {
        console.log(e);
      });
  };
  useEffect(() => {
    fetch();
  }, []);
  return (
    <View style={{backgroundColor: '#FAAF51', height: 610, display: 'flex'}}>
      {/* <Button
        title="Go to Jane's profile"
        onPress={() => navigation.navigate('Add', {name: 'jan'})}
      /> */}
      <TextInput
        // underlineColorAndroid={'gray'}
        value={todo_}
        placeholder="New Todo"
        onChangeText={(e) => setTodo(e)}
        style={styles.text_input}
      />

      <View style={styles.todo_container}>
        <TouchableOpacity onPress={() => submit()}>
          <View style={styles.button_}>
            <Text style={styles.add_text}>Add Todo </Text>
          </View>
        </TouchableOpacity>

        {todo_list.length <= 0 ? <Text>No Todos... </Text> : render_list()}
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  todo_container: {
    // backgroundColor: '#FAAF51',
    height: 520,
    marginLeft: 8,
    marginRight: 8,
  },

  button_: {
    marginLeft: 15,
    marginRight: 15,
    marginTop: 10,
    marginBottom: 2,
    borderRadius:20,
    backgroundColor: 'red',
    height: 50,
    justifyContent: 'center',
    display: 'flex',
    alignItems: 'center',
    flexDirection: 'row',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.23,
    shadowRadius: 2.62,
    elevation: 4,
  },
  todo: {
    marginLeft: 15,
    marginRight: 15,
    marginTop: 15,
    marginBottom: 2,
    paddingLeft: 50,
    backgroundColor: '#fff',
    height: 60,
    display: 'flex',
    alignItems: 'center',
    flexDirection: 'row',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.23,
    shadowRadius: 2.62,
    elevation: 4,
  },
  todo_text: {
    color: 'black',
    fontSize: 20,
    paddingLeft: 20,
  },
  add_text: {
    color: 'white',
    fontSize: 20,
    // paddingLeft:20
  },
  container: {
    // backgroundColor: 'red',
    height: 50,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  header_text: {
    fontSize: 17,
    fontWeight: 'bold',
    color: 'white',
  },
  text_input: {
    marginTop: 10,
    fontSize: 18,
    color: 'black',
    backgroundColor: 'white',
    marginLeft: 23,
    height: 60,
    marginRight: 23,
  },
  button_login: {
    marginTop: 5,
    marginLeft: 60,
    display: 'flex',
    alignItems: 'center',
    width: 300,
    backgroundColor: 'red',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
  },
});

export default Dashboard;
