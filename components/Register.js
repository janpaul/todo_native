import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  StyleSheet,
  TextInput,
  Image,
  Button,
  Alert,
  TouchableOpacity,
  AsyncStorage,
} from 'react-native';
import {register as register_user} from './Url';
import Toast from 'react-native-toast-message'
const Register = ({navigation}) => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [name, setName] = useState('');

  const register = async () => {
    register_user(email,password,name).then( (r)=>{
        alert(r.data.message);
        navigation.navigate('Login');
    })

    
    Toast.show({
        text1: 'Hello',
        text2: 'This is some something 👋'
      });
  };
  const alert_ = (title, message) =>
    Alert.alert(
      title,
      message,
      [
        {text: 'OK', onPress: () => console.log('OK Pressed')},
      ],
      {cancelable: false},
    );

  useEffect(() => {}, []);

  return (
    <View>
          <Toast ref={(ref) => Toast.setRef(ref)} />
      <View style={{backgroundColor: '#FAAF51', height: 250}}></View>
      <View style={styles.container}>
        <View style={styles.login_pic}>
          <Image
            style={{width: 200, height: 200, borderRadius: 100}}
            source={{
              uri:
                'https://www.myclientsplus.com/media/1007/task2fpractice-management.png',
            }}
          />
        </View>
        <View style={styles.input_wrapper}>
          <TextInput
            underlineColorAndroid={'gray'}
            placeholder="Name"
            onChangeText={(e) => setName(e)}
            style={styles.text_input}
            value={name}
          />
        </View>
        <View style={styles.input_wrapper}>
          <TextInput
            underlineColorAndroid={'gray'}
            placeholder="Email"
            onChangeText={(e) => setEmail(e)}
            style={styles.text_input}
            value={email}
          />
        </View>

        <View style={styles.input_wrapper}>
          <TextInput
            placeholder="Password"
            underlineColorAndroid={'gray'}
            onChangeText={(e) => setPassword(e)}
            value={password}
            secureTextEntry={true}
            style={styles.text_input}
          />
        </View>

        <View>
          <TouchableOpacity
            style={styles.button_login}
            onPress={() => register()}>
            <Text style={{color: 'white', fontWeight: 'bold', fontSize: 15}}>
              Register
            </Text>
          </TouchableOpacity>
        </View>
      </View>
      


  
    </View>
  );
};

const styles = StyleSheet.create({
  text_register: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: 15,
  },
  button_register: {
    display: 'flex',
    alignItems: 'center',
    width: 150,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.23,
    shadowRadius: 2.62,
    elevation: 4,
    backgroundColor: '#FAAF51',
    // borderRadius: 5,
    paddingVertical: 10,
    paddingHorizontal: 12,
    marginBottom: 30,
  },
  register_containter: {
    display: 'flex',
    alignItems: 'center',
    marginTop: 30,
  },
  button_login: {
    display: 'flex',
    alignItems: 'center',
    width: 300,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.23,
    shadowRadius: 2.62,
    elevation: 4,
    backgroundColor: '#FAAF51',
    // borderRadius: 5,
    paddingVertical: 10,
    paddingHorizontal: 12,
    marginBottom: 30,
  },
  container: {
    display: 'flex',
    alignItems: 'center',
    paddingLeft: 5,
    paddingRight: 5,
    backgroundColor: '#fff',
    marginLeft: 20,
    marginRight: 20,
    marginTop: -190,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.23,
    shadowRadius: 2.62,

    elevation: 4,
  },
  login_pic: {
    alignItems: 'center',
    display: 'flex',
    padding: 15,
  },
  input_wrapper: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    padding: 15,
  },
  text_input: {
    height: 40,

    flex: 1,
  },
  text_label: {marginRight: 5, fontWeight: 'bold', width: 80, fontSize: 18},
});

export default Register;
