import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  Image,
  Button,
  StatusBar,
} from 'react-native';

const AddTodo = () => {
  return (
    <View>
      <Text> add to do here</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'red',
    height: 50,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  header_text: {
    fontSize: 17,
    fontWeight: 'bold',
    color: 'white',
  },
});

export default AddTodo;
