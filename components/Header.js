import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  Image,
  StatusBar,
} from 'react-native';

const Header = () => {
  return (
    <View style={styles.container}>
      <Text style={styles.header_text}>To do List</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'red',
    height: 50,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  header_text: {
    fontSize: 17,
    fontWeight: 'bold',
    color: 'white',
  },
});

export default Header;
