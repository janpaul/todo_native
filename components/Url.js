import axios from 'axios';

 
let request =(token = "")=>{
   return axios.create({
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer` + token 
        },
      });
}

let url = 'https://43723ae84204.ngrok.io/' + 'api';

export const login = async (email, password) => {
  return request().post(url + '/login', {
    email: email,
    password: password,
  });
};

export const register = async (email, password,name) => {
  return request().post(url + '/user-mobile', {
    email: email,
    password: password,
    name:name,
  });
};

export const todo = (token) =>{
    return request(token).get(url + '/todos')
}

export const create = (token,todo,date) =>{
    return request(token).post(url + '/todos',{
        todo:todo
    })
}

export const update = (token,id) =>{
    return request(token).patch(url + '/todos/' + id,{

    })
}